class CreateJoinTableUnitsYears < ActiveRecord::Migration[6.0]
  def change
    create_join_table :units, :years do |t|
      t.index %i[unit_id year_id]
      t.index %i[year_id unit_id]
    end
  end
end
