class CreateInfos < ActiveRecord::Migration[6.0]
  def change
    create_table :infos do |t|
      t.string :entity
      t.string :text_value

      t.timestamps
    end
  end
end
