class CreateSocials < ActiveRecord::Migration[6.0]
  def change
    create_table :socials do |t|
      t.string :entity
      t.string :icon
      t.string :url

      t.timestamps
    end
  end
end
