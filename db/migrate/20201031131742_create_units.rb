class CreateUnits < ActiveRecord::Migration[6.0]
  def change
    create_table :units do |t|
      t.string :unit
      t.string :grade
      t.string :description

      t.timestamps
    end
  end
end
