class CreateHighlights < ActiveRecord::Migration[6.0]
  def change
    create_table :highlights do |t|
      t.string :title
      t.string :organisation
      t.date :start_date
      t.date :end_date
      t.string :description
      t.string :url

      t.timestamps
    end
  end
end
