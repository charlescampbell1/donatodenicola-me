module.exports = config = {};

config.bumpFiles = [
  {
    filename: "VERSION",
    type: "plain-text"
  }
];

const host = "https://gitlab.com/charlescampbell1/donatodenicola-me";

config.commitUrlFormat = `${host}/donatodenicola-me/commit/{{hash}}`;

config.compareUrlFormat = `${host}/-/compare/{{previousTag}}...{{currentTag}}`;
