# frozen_string_literal: true

Rails.application.routes.draw do
  root 'highlights#index'

  get 'highlights', to: 'highlights#index', as: :highlights
  get 'backend', to: 'highlights#backend', as: :backend

  scope :highlights do
    get 'new', to: 'highlights#new', as: :new_highlight
    get 'edit/:id', to: 'highlights#edit', as: :edit_highlight
    post 'create', to: 'highlights#create', as: :create_highlight
    get 'backend', to: 'highlights#backend', as: :backend_highlights
    patch 'update/:id', to: 'highlights#update', as: :update_highlight
    delete 'delete/:id', to: 'highlights#destroy', as: :delete_highlight
  end

  get 'education', to: 'education#index', as: :education

  scope :education do
    get 'backend', to: 'education#backend', as: :backend_education

    scope :qualification do
      get 'new', to: 'education#new_qualification', as: :new_qualification
      post 'create', to: 'education#create_qualification', as: :create_qualification
      get 'show/:id', to: 'education#show_qualification', as: :show_qualification
      get 'edit/:id', to: 'education#edit_qualification', as: :edit_qualification
      patch 'update/:id', to: 'education#update_qualification', as: :update_qualification
      delete 'delete/:id', to: 'education#destroy_qualification', as: :delete_qualification
    end

    scope :year do
      get 'new/:qualification', to: 'education#new_year', as: :new_year
      post 'create/:qualification', to: 'education#create_year', as: :create_year
      get 'show/:id', to: 'education#show_year', as: :show_year
      get 'edit/:id', to: 'education#edit_year', as: :edit_year
      patch 'update/:id', to: 'education#update_year', as: :update_year
      delete 'delete/:id', to: 'education#destroy_year', as: :delete_year
    end

    scope :unit do
      get 'new/:year', to: 'education#new_unit', as: :new_unit
      post 'create/:year', to: 'education#create_unit', as: :create_unit
      get 'show/:id', to: 'education#show_unit', as: :show_unit
      get 'edit/:id', to: 'education#edit_unit', as: :edit_unit
      patch 'update/:id', to: 'education#update_unit', as: :update_unit
      delete 'delete/:id', to: 'education#destroy_unit', as: :delete_unit
    end
  end

  get 'profile', to: 'intro#backend', as: :profile

  scope :profile do
    scope :information do
      get 'new', to: 'intro#new_info', as: :new_info
      post 'create', to: 'intro#create_info', as: :create_info
      get 'edit/:id', to: 'intro#edit_info', as: :edit_info
      patch 'update/:id', to: 'intro#update_info', as: :update_info
      delete 'delete/:id', to: 'intro#destroy_info', as: :delete_info
    end

    scope :social_link do
      get 'new', to: 'intro#new_social', as: :new_social
      post 'create', to: 'intro#create_social', as: :create_social
      get 'edit/:id', to: 'intro#edit_social', as: :edit_social
      patch 'update/:id', to: 'intro#update_social', as: :update_social
      delete 'delete/:id', to: 'intro#destroy_social', as: :delete_social
    end
  end
end
