# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [1.1.1](https://gitlab.com/charlescampbell1/donatodenicola-me/-/compare/v1.1.0...v1.1.1) (2020-11-04)


### Bug Fixes

* **highlight:** Resolve issue where users couldn't post present date ([043a63d](https://gitlab.com/charlescampbell1/donatodenicola-me/donatodenicola-me/commit/043a63d906d5900462555a17434f19f729c13247))

## [1.1.0](https://gitlab.com/charlescampbell1/donatodenicola-me/-/compare/v1.0.4...v1.1.0) (2020-11-03)


### Features

* **design:** Update header background to network cables ([07e02c8](https://gitlab.com/charlescampbell1/donatodenicola-me/donatodenicola-me/commit/07e02c88e1db4e605c83623906c41d51b80f6391))

### [1.0.4](https://gitlab.com/charlescampbell1/donatodenicola-me/-/compare/v1.0.3...v1.0.4) (2020-11-02)

### [1.0.3](https://gitlab.com/charlescampbell1/donatodenicola-me/-/compare/v1.0.2...v1.0.3) (2020-11-01)


### Bug Fixes

* **education:** Removed issue where users couldn't add new records of academic years ([2e99dd9](https://gitlab.com/charlescampbell1/donatodenicola-me/donatodenicola-me/commit/2e99dd9b322fc261ef1afe803196c5dd58c835ff))

### [1.0.2](https://gitlab.com/charlescampbell1/donatodenicola-me/-/compare/v1.0.1...v1.0.2) (2020-11-01)

### [1.0.1](https://gitlab.com/charlescampbell1/donatodenicola-me/-/compare/v1.0.0...v1.0.1) (2020-11-01)

### [1.0.0](https://gitlab.com/charlescampbell1/donatodenicola-me/compare/v0.1.0...v1.0.0) (2020-11-01)


### Features

* **education:** Users can add details about their education ([b5046a4](https://gitlab.com/charlescampbell1/donatodenicola-me/commit/b5046a43daca8efbb242b0bc7bb002c8b4ec5ce5))
* **intro:** Users can now edit their social icons, name and position ([d422b18](https://gitlab.com/charlescampbell1/donatodenicola-me/commit/d422b18e5485e08b5aea9bf8edcb97d61161940c))

## 0.1.0 (2020-10-31)


### Features

* **highlights:** Added authentication for backend options ([c4ea2b8](https://gitlab.com/charlescampbell1/donatodenicola-me/commit/c4ea2b83de53563df9d4070513702866f8bb3cc9))
* **highlights:** Space for users to add, remove, update and view highlights ([947d694](https://gitlab.com/charlescampbell1/donatodenicola-me/commit/947d694240b564fdd6051a554f840a34ebc0ed8a))
