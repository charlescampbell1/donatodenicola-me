# frozen_string_literal: true

module StyleHelper
  def page_title(title:, subtitle: '')
    render 'frontend/components/page_title', title: title, subtitle: subtitle
  end
end
