# frozen_string_literal: true

module ApplicationHelper
  def version
    "v#{File.read('./VERSION')}"
  end
end
