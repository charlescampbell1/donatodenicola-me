# frozen_string_literal: true

module IntroHelper
  def name
    info = Info.where(entity: 'name').first

    return '' if info.nil?

    info.text_value
  end

  def position
    info = Info.where(entity: 'position').first

    return '' if info.nil?

    info.text_value
  end

  def social_icons
    Social.all
  end
end
