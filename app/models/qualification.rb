# frozen_string_literal: true

class Qualification < ApplicationRecord
  has_and_belongs_to_many :years
end
