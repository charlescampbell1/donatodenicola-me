# frozen_string_literal: true

class Highlight < ApplicationRecord
  validates_presence_of :title, :organisation, :start_date, :description
end
