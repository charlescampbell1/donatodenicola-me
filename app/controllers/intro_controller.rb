# frozen_string_literal: true

class IntroController < ApplicationController
  layout 'backend'

  before_action :set_info, only: %i[edit_info update_info]
  before_action :set_social, only: %i[edit_social update_social destroy_social]

  http_basic_authenticate_with name: ENV['USERNAME'], password: ENV['PASSWORD']

  def backend
    @info = Info.all
    @socials = Social.all

    render 'backend/intro/index'
  end

  def new_info
    @info = Info.new

    render 'backend/intro/info/new'
  end

  def create_info
    @info = Info.new(info_params)

    respond_to do |format|
      if @info.save
        format.html { redirect_to profile_path, notice: 'Created intro item' }
      else
        format.html { redirect_to profile_path, alert: 'Something went wrong' }
      end
    end
  end

  def edit_info
    render 'backend/intro/info/edit'
  end

  def update_info
    respond_to do |format|
      if @info.update(info_params)
        format.html { redirect_to profile_path, notice: 'Information updated' }
      else
        format.html { redirect_to profile_path, notice: 'Something went wrong' }
      end
    end
  end

  def new_social
    @social = Social.new

    render 'backend/intro/social/new'
  end

  def create_social
    @social = Social.new(social_params)

    respond_to do |format|
      if @social.save
        format.html { redirect_to profile_path, notice: 'Created intro item' }
      else
        format.html { redirect_to profile_path, alert: 'Something went wrong' }
      end
    end
  end

  def edit_social
    render 'backend/intro/social/edit'
  end

  def update_social
    respond_to do |format|
      if @social.update(social_params)
        format.html { redirect_to profile_path, notice: 'Information updated' }
      else
        format.html { redirect_to profile_path, notice: 'Something went wrong' }
      end
    end
  end

  def destroy_social
    @social.destroy

    redirect_to profile_path, notice: 'Social icon deleted.'
  end

  private

  def info_params
    params.require(:info).permit(:entity, :text_value)
  end

  def social_params
    params.require(:social).permit(:entity, :icon, :url)
  end

  def set_info
    @info = Info.find(params[:id])
  end

  def set_social
    @social = Social.find(params[:id])
  end
end
