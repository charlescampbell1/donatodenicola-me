# frozen_string_literal: true

class HighlightsController < ApplicationController
  layout 'backend', only: %i[backend show edit update destroy]

  before_action :set_highlight, only: %i[show edit update destroy]
  before_action :set_highlights, only: %i[index backend]

  http_basic_authenticate_with name: ENV['USERNAME'], password: ENV['PASSWORD'], only: %i[backend new edit]

  def index
    render 'frontend/highlights/index'
  end

  def backend
    render 'backend/highlights/index'
  end

  def new
    @highlight = Highlight.new

    render 'backend/highlights/new'
  end

  def edit
    render 'backend/highlights/edit'
  end

  def create
    @highlight = Highlight.new(highlight_params)

    respond_to do |format|
      if @highlight.save
        format.html { redirect_to backend_highlights_path, notice: 'highlight created' }
      else
        format.html { redirect_to backend_highlights_path, notice: 'failed to create' }
      end
    end
  end

  def update
    respond_to do |format|
      if @highlight.update(highlight_params)
        format.html { redirect_to backend_highlights_path, notice: 'highlight updated' }
      else
        format.html { redirect_to backend_highlights_path, notice: 'failed to update' }
      end
    end
  end

  def destroy
    @highlight.destroy
    respond_to do |format|
      format.html { redirect_to backend_highlights_path, notice: 'highlight removed' }
    end
  end

  private

  def set_highlights
    @highlights = Highlight.all
  end

  def set_highlight
    @highlight = Highlight.find(params[:id])
  end

  def highlight_params
    params.require(:highlight).permit(:title, :organisation, :start_date, :end_date, :description, :url)
  end
end
