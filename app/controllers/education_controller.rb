# frozen_string_literal: true

class EducationController < ApplicationController
  layout 'backend', only: %i[backend show_qualification edit_qualification new_qualification new_year edit_year new_unit edit_unit]

  before_action :set_qualifications, only: %i[index backend]
  before_action :set_qualification, only: %i[show_qualification edit_qualification update_qualification destroy_qualification]
  before_action :set_year, only: %i[edit_year update_year destroy_year]
  before_action :set_unit, only: %i[edit_unit update_unit destroy_unit]

  http_basic_authenticate_with name: ENV['USERNAME'], password: ENV['PASSWORD'],
                               only: %i[backend new_qualification new_year new_unit]

  def index
    render 'frontend/education/index'
  end

  def backend
    render 'backend/education/index'
  end

  def show_qualification
    render 'backend/education/qualification/show'
  end

  def new_qualification
    @qualification = Qualification.new

    render 'backend/education/qualification/new'
  end

  def create_qualification
    @qualification = Qualification.new(qualification_params)

    respond_to do |format|
      if @qualification.save
        format.html { redirect_to show_qualification_path(@qualification), notice: 'You can now add more details' }
      else
        format.html { redirect_to backend_education_path, alert: 'Something went wrong' }
      end
    end
  end

  def edit_qualification
    render 'backend/education/qualification/edit'
  end

  def update_qualification
    respond_to do |format|
      if @qualification.update(qualification_params)
        format.html { redirect_to show_qualification_path(@qualification), notice: 'Qualification updated' }
      else
        format.html { redirect_to backend_education_path, notice: 'Something went wrong' }
      end
    end
  end

  def destroy_qualification
    @qualification.destroy

    redirect_to backend_education_path, notice: 'Qualification deleted'
  end

  def new_year
    @year = Year.new

    render 'backend/education/year/new'
  end

  def create_year
    @year = Year.new(year_params)

    @qualification = Qualification.find(params[:qualification])

    respond_to do |format|
      if @year.save
        @qualification.years << @year
        format.html { redirect_to show_qualification_path(@qualification), notice: 'You can now add more details' }
      else
        format.html { redirect_to backend_education_path, alert: 'Something went wrong' }
      end
    end
  end

  def edit_year
    render 'backend/education/year/edit'
  end

  def update_year
    respond_to do |format|
      if @year.update(year_params)
        format.html { redirect_to show_qualification_path(@year.qualifications.first), notice: 'Qualification updated' }
      else
        format.html { redirect_to backend_education_path, notice: 'Something went wrong' }
      end
    end
  end

  def destroy_year
    @qualification = @year.qualifications.first
    @year.destroy

    redirect_to show_qualification_path(@qualification), notice: 'Year deleted'
  end

  def new_unit
    @unit = Unit.new

    render 'backend/education/unit/new'
  end

  def create_unit
    @unit = Unit.new(unit_params)
    @year = Year.find(params[:year])

    respond_to do |format|
      if @unit.save
        @year.units << @unit
        format.html { redirect_to show_qualification_path(@year.qualifications.first), notice: 'You can now add more details' }
      else
        format.html { redirect_to backend_education_path, alert: 'Something went wrong' }
      end
    end
  end

  def edit_unit
    render 'backend/education/unit/edit'
  end

  def update_unit
    respond_to do |format|
      if @unit.update(unit_params)
        format.html { redirect_to show_qualification_path(@unit.years.first.qualifications.first), notice: 'Qualification updated' }
      else
        format.html { redirect_to backend_education_path, notice: 'Something went wrong' }
      end
    end
  end

  def destroy_unit
    @qualification = @unit.years.first.qualifications.first
    @unit.destroy

    redirect_to show_qualification_path(@qualification), notice: 'Unit deleted'
  end

  private

  def qualification_params
    params.require(:qualification).permit(:title, :organisation)
  end

  def set_qualification
    @qualification = Qualification.find(params[:id])
  end

  def set_qualifications
    @qualifications = Qualification.all
  end

  def year_params
    params.require(:year).permit(:title, :grade)
  end

  def set_year
    @year = Year.find(params[:id])
  end

  def unit_params
    params.require(:unit).permit(:unit, :grade, :description)
  end

  def set_unit
    @unit = Unit.find(params[:id])
  end
end
